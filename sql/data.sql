use intro

Insert into SchoolClass (Id, Name, YearStart) values(1, 'AP22a', 2022);
Insert into SchoolClass (Id, Name, YearStart) values(2, 'AP21a', 2021);

Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (1,'Huberta','Jurk', 1);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (2,'Phillip','Teich', 1);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (3,'Torben','Hansel', 1);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (4,'Ekkehart','Eckmann', 1);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (5,'Laurentius','Elze', 1);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (6,'Celia','Ludolph', 2);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (7,'Dustin','Guggemos', 2);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (8,'Ferdinand','Gutierrez', 2);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (9,'Wilderich','Wallat', 2);
Insert into Student (Id, Firstname, Lastname, ClassId) 
	values (10,'Valerian','Steding', 2);

Insert into Teacher (Id, Firstname, Lastname) 
	values (1,'Amalia','Zech');
Insert into Teacher (Id, Firstname, Lastname) 
	values (2,'Valentine','Pollak');
Insert into Teacher (Id, Firstname, Lastname) 
	values (3,'Margareta','Kraska');
Insert into Teacher (Id, Firstname, Lastname) 
	values (4,'Reinbert','Bretthauer');
	
Insert into ItModule (Id, Name, Description)
	values (1, 'm162', 'Daten analysieren und modellieren');
Insert into ItModule (Id, Name, Description)
	values (2, 'm164', 'Datenbanken erstellen und Daten einfügen');
Insert into ItModule (Id, Name, Description)
	values (3, 'm165', 'NoSQL-Datenbanken einsetzen');
Insert into ItModule (Id, Name, Description)
	values (4, 'm319', 'Applikationen entwerfen und implementieren');
Insert into ItModule (Id, Name, Description)
	values (5, 'm320', 'Objektorientiert Programmieren');
Insert into ItModule (Id, Name, Description)
	values (6, 'm322', 'Benutzerschnittstellen entwerfen und implementieren');
	
insert into Grade (StudentId, ModuleId, Grade) values (1,1,4.5);
insert into Grade (StudentId, ModuleId, Grade) values (1,2,5);
insert into Grade (StudentId, ModuleId, Grade) values (1,3,2.3);
insert into Grade (StudentId, ModuleId, Grade) values (1,4,3);
insert into Grade (StudentId, ModuleId, Grade) values (1,5,4.5);
insert into Grade (StudentId, ModuleId, Grade) values (1,6,5);
insert into Grade (StudentId, ModuleId, Grade) values (2,1,6);
insert into Grade (StudentId, ModuleId, Grade) values (2,2,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (2,3,4.2);
insert into Grade (StudentId, ModuleId, Grade) values (2,4,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (2,5,5.4);
insert into Grade (StudentId, ModuleId, Grade) values (2,6,3.3);
insert into Grade (StudentId, ModuleId, Grade) values (3,1,4.3);
insert into Grade (StudentId, ModuleId, Grade) values (3,2,5.3);
insert into Grade (StudentId, ModuleId, Grade) values (3,3,2.8);
insert into Grade (StudentId, ModuleId, Grade) values (3,4,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (3,5,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (3,6,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (4,1,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (4,2,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (4,3,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (4,4,3.3);
insert into Grade (StudentId, ModuleId, Grade) values (4,5,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (4,6,3.8);
insert into Grade (StudentId, ModuleId, Grade) values (5,1,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (5,2,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (5,3,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (5,4,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (5,5,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (5,6,5.7);
insert into Grade (StudentId, ModuleId, Grade) values (6,1,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (6,2,5.5);
insert into Grade (StudentId, ModuleId, Grade) values (6,3,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (6,4,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (6,5,5.5);
insert into Grade (StudentId, ModuleId, Grade) values (6,6,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (7,1,6);
insert into Grade (StudentId, ModuleId, Grade) values (7,2,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (7,3,5.3);
insert into Grade (StudentId, ModuleId, Grade) values (7,4,5.4);
insert into Grade (StudentId, ModuleId, Grade) values (7,5,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (7,6,4.4);
insert into Grade (StudentId, ModuleId, Grade) values (8,1,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (8,2,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (8,3,4.6);
insert into Grade (StudentId, ModuleId, Grade) values (8,4,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (8,5,6);
insert into Grade (StudentId, ModuleId, Grade) values (8,6,3.5);
insert into Grade (StudentId, ModuleId, Grade) values (9,1,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (9,2,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (9,3,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (9,4,5.1);
insert into Grade (StudentId, ModuleId, Grade) values (9,5,5.2);
insert into Grade (StudentId, ModuleId, Grade) values (9,6,4.9);
insert into Grade (StudentId, ModuleId, Grade) values (10,1,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (10,2,4);
insert into Grade (StudentId, ModuleId, Grade) values (10,3,4.8);
insert into Grade (StudentId, ModuleId, Grade) values (10,4,4.1);
insert into Grade (StudentId, ModuleId, Grade) values (10,5,5);
insert into Grade (StudentId, ModuleId, Grade) values (10,6,5.2);

insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (1,1,1);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (1,2,2);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (1,3,3);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (1,4,4);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (1,5,1);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (1,6,2);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (2,1,3);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (2,2,4);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (2,3,1);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (2,4,2);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (2,5,1);
insert into TeachingArrangement (ClassId, ModuleId, TeacherId) values (2,6,1);