#!/bin/bash

sudo mysql -sfu root <<EOS
-- set root password
UPDATE mysql.user SET Password=PASSWORD('password') WHERE User='root';
-- delete anonymous users
DELETE FROM mysql.user WHERE User='';
-- delete remote root capabilities
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
-- drop database 'test'
DROP DATABASE IF EXISTS test;
-- also make sure there are lingering permissions to it
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
-- create new user admin
GRANT ALL ON *.* TO 'admin'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;
-- make changes immediately
FLUSH PRIVILEGES;
EOS

#sudo mysql -u username -p database_name < file.sql
# create the database
sudo mysql -sfu root <<EOS
drop database if exists intro;
create database intro;
-- make changes immediately
FLUSH PRIVILEGES;
EOS

# create structure of database
sudo mysql -fu root < /home/ubuntu/m165_scripts/mongodb/structure.sql
# create content of database
sudo mysql -fu root < /home/ubuntu/m165_scripts/mongodb/data.sql