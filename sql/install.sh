#!/bin/bash
sudo apt-get update -y

# =============== Maria DB
sudo apt-get -y install mariadb-server
sudo systemctl start mariadb.service # start as process
#sudo systemctl status mariadb.service
sudo systemctl enable mariadb.service # start maria after reboot
# replace binding, so the database can be access from remote
sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mariadb.conf.d/50-server.cnf
sudo systemctl restart mariadb.service

# give permission to execute
chmod +x /home/ubuntu/m165_scripts/mongodb/sql.sh
# execute sql script
/home/ubuntu/m165_scripts/mongodb/sql.sh

# =============== Mongo DB. WORKS ONLY FOR UBUNTU 20.4 !!!
sudo apt-get install gnupg
# import the MongoDB public GPG Key
wget -qO - https://www.mongodb.org/static/pgp/server-6.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/6.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list
sudo apt-get update -y
sudo apt-get install -y mongodb-org
sudo systemctl start mongod # start mongodb as process
#sudo systemctl status mongod
sudo systemctl enable mongod # start mongodb after reboot

# configure remote access
sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mongod.conf
sudo systemctl restart mongod

# give permission to execute
chmod +x /home/ubuntu/m165_scripts/mongodb/nosql.sh
# execute sql script
/home/ubuntu/m165_scripts/mongodb/nosql.sh
